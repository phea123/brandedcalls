# Branded Calls - GOSAT #

Este é um AGI criado em Python para auxiliar no processo de utilização da API do serviço Branded Calls da GOSAT.

### API ###
* [Documentação Oficial](https://brandedcall.gosat.org/api/docs).

### Dependências ###

* pyst2
* requests
* stdiomask
* pathlib

### Setup ###
* Clone o projeto no diretório `/opt`.
* Crie o ambiente virtual: `python3 -m venv /opt/brandedcalls/`

### Utilização ###
    Uso: ./agi_gbcall.py [OPÇÕES]

    Opções disponíveis
        --motivos - Lista todos os motivos de ligação disponíveis para uso (JSON).
        --help    - Apresenta esse menu.

### Autenticação ###

Após clonar o projeto, acesse a pasta e execute o script a primeira vez. Será solicitado o usuário e senha para autenticação. Concluindo será gerado o arquivo `access_token.json` que conterá seus dados de autenticação que serão utilizados nas requisições da API. 

Para testar o AGI, execute o script passando o argumento `--motivos`. Essa execução deverá retornar um JSON contendo os motivos de ligações disponíveis para uso.

###  Permissões ###
O usuário utilizado pelo Asterisk deverá ter permissão de acesso ao script.

Para correto funcionamento o script deverá ter permissão de execução, para isso execute:  
```chmod +x agi_gbcall.py```

### Link simbólico ###
Crie o link simbólico para que o Asterisk reconheça o script como um AGI:  
```ln -s /opt/brandedcalls/agi_gbcall.py /var/lib/asterisk/agi-bin/```

### Asterisk Dialplan ###

- Exemplo de utilização no Asterisk.
```Asterisk
exten => _X.,1,NoOp(Branded Call)
same  => n,Set(origem=551130031112)
same  => n,Set(destino=${EXTEN})
same  => n,Set(motivo=f26bba68-7a8f-4c98-99e1-e3ab01c5a711)
same  => n,Agi(agi_gbcall.py)
same  => n,Dial(SIP/GOSAT/${EXTEN},,o)
same  => n,Hangup()
```
