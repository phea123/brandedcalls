#!/opt/brandedcalls/bin/python

def help():
    helper = """

* ./agi_gbcall.py [OPÇÕES]

Opções disponíveis
    --motivos - Lista todos os motivos de ligação disponíveis para uso (JSON).
    --help    - Apresenta esse menu.

    """
    print(helper)
    exit()


def coletarArgs():
    # Coleta de argumentos
    if '--help' in sys.argv:
        help()

    if '--motivos' in sys.argv:
        motivos = True
    else:
        motivos = False
    
    retorno = {
        'motivos': motivos,
    }

    return retorno


"""
Validação de dependências
"""
def init(pacotes):
    import os
    for pacote in pacotes:
        try:
            if pacote == 'pyst2':
                __import__('asterisk.agi')
            else:
                __import__(pacote)
        except:
            print('Instalando: ' + pacote)
            os.system(pathAgi + "/bin/pip --disable-pip-version-check install -q " + pacote)


"""
Verificando arquivo de autenticação
Retorna `access_token` em caso de sucesso
Retorna `None` em caso de erro
"""
def autenticacao(arquivoJson):
    try:
        arquivoJson.resolve(arquivoJson)
    except FileNotFoundError:
        return None
    else:
        jsonFile = open(arquivoJson)
        jsonData = json.load(jsonFile)
        return jsonData['access_token']


"""
Requisições à API
"""
def api(url = None, metodo = 'GET', data = None, chave = None):
    # Preparando headers para chamada da API
    if chave is not None:
        headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer " + str(chave)
        }
    else:
        headers = {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }

    # Enviando requisição para a API
    if metodo == 'GET':
        retorno = requests.get(url, headers=headers, timeout=10)
    elif metodo == 'POST':
        retorno = requests.post(url, headers=headers, data=json.dumps(data), timeout=10)
    else:
        return False
    return retorno


"""
Tratativa de telefones para padronização E.164
"""
def tratarTelefone(telefone):
    # Trativa do telefone do cliente para padronização E.164
    try:
        # 0 + Operadora + DDD + Número
        if telefone[0:3] == '021':
            telefoneTratado = '55' + telefone[3:]
                # 0 + Operadora + DDD + Número
        else:
            if len(telefone) < 12 and len(telefone) > 9:
                telefoneTratado = '55' + telefone
            elif len(telefone) == 12:
                telefoneTratado = '55' + telefone[1:]
            elif len(telefone) == 14:
                telefoneTratado = telefone[1:]
            else:
                telefoneTratado = telefone
    except:
        return None
    return telefoneTratado


"""
Coleta as variáveis na sessão atual do Asterisk
telefoneOrigem = Número de A
telefoneDestino = Número de B
motivoLigacao = ID do motivo desejado
"""
def coletarDadosAsterisk():
    agi = AGI()
    telefoneOrigem = tratarTelefone(agi.get_variable('origem'))
    telefoneDestino = tratarTelefone(agi.get_variable('destino'))
    motivoLigacao = agi.get_variable('motivo') if len(agi.get_variable('motivo')) > 0 else None

    retorno = {
        "telefoneOrigem": telefoneOrigem,
        "telefoneDestino": telefoneDestino,
        "motivoLigacao": motivoLigacao
    }
    return retorno


"""
Gera o arquivo de autenticação (Json)
"""
def gerarChaveAcesso(arquivoAuth):
    import stdiomask
    print('\nPara utilização deste AGI é necessário gerar um arquivo de autenticação. \nPara isso informe o usuário e senha fornecidos pela GOSAT via e-mail.\n')
    usuario = input("Usuário: ")
    senha = stdiomask.getpass("Senha: ")
    url = """https://brandedcall.gosat.org/api/v1/login"""
    data = {
        "username": usuario,
        "password": senha,
        "remember_me": True
    }
    loginApi = api(url=url, metodo='POST', data=data)
    if loginApi.status_code == 200:
        arquivo = open(arquivoAuth, "w")
        retorno = json.dumps(loginApi.json(), sort_keys=True, indent=4)
        arquivo.write(retorno)
        arquivo.close()
        return True
    else:
        print("\n* Acesso inválido! Verifique o usuário/senha informados e tente novamente.\n")
    return False


"""
Inicialização do script
"""
if __name__ == '__main__':
    # Path
    pathAgi = "/opt/brandedcalls"
    # pathAgi = "/Volumes/Phelipe_Rodrigues/GIT/brandedcalls/brandedcall"

    # Validação de dependências
    init(['requests', 'pyst2', 'json', 'stdiomask', 'pathlib'])
    try:
        import requests, json, sys
        from pathlib import Path
        from asterisk.agi import AGI
    except:
        print('Não foi possível instalar ou carregar as dependencias necessárias. Entre em contato com nossa equipe de Suporte.')
        exit()

    # Coleta argumentos passados via terminal
    argumentos = coletarArgs()

    # Validação da chave de acesso
    arquivoJson = Path(pathAgi + "/access_key.json")
    chaveAcessoApi = autenticacao(arquivoJson)
    if chaveAcessoApi is None:
        retorno = gerarChaveAcesso(arquivoJson)
        if retorno:
            print('\n* Arquivo de autenticação gerado com sucesso!\n')
            exit()
        else:
            exit()

    if argumentos['motivos']:
        # Apresenta os motivos de ligações disponíveis - Formato Json
        urlBuscarMotivos = """https://brandedcall.gosat.org/api/v1/call-reason"""
        retorno = api(url=urlBuscarMotivos, chave=chaveAcessoApi)
        motivos = retorno.json()
        if len(motivos) < 1:
            print('Nenhum motivo de ligação cadastrado.')
        else:
            print(json.dumps(motivos, sort_keys=False, indent=4))
        exit()

    # Coletando os dados necessários do Asterisk
    dados = coletarDadosAsterisk()

    # Validando variáveis coletadas do Asterisk
    # Caso não seja informado um motivo da ligação
    # É coletado via API o primeiro motivo disponível
    if dados['telefoneOrigem'] is None or dados['telefoneDestino'] is None:
        print('Parametros não encontrados na sessão.')
        exit()
    elif dados['motivoLigacao'] is None or dados['motivoLigacao'] == '':
        urlBuscarMotivos = """https://brandedcall.gosat.org/api/v1/call-reason"""
        retorno = api(url=urlBuscarMotivos, chave=chaveAcessoApi)
        motivos = retorno.json()
        dados['motivoLigacao'] = motivos[0]['id']
    if len(dados['telefoneDestino']) == 9:
        dados['telefoneDestino'] = '55' + dados['telefoneOrigem'][2:4] + dados['telefoneDestino']

    # Registrando a ligação via API
    url = """https://brandedcall.gosat.org/api/v1/register-call"""
    data = {
        "source_number": dados['telefoneOrigem'],
        "destination_number": dados['telefoneDestino'],
        "reason_id": dados['motivoLigacao']
    }
    registrarLigacao = api(url=url, metodo='POST', data=data, chave=chaveAcessoApi)

    if registrarLigacao.status_code > 499 and registrarLigacao.status_code < 600:
        # Retry em caso de erro HTTP da faixa 5XX
        registrarLigacao = api(url=url, metodo='POST', data=data, chave=chaveAcessoApi)
    elif registrarLigacao.status_code == 401 or registrarLigacao.status_code == 403:
        # Usuário bloqueado ou token expirado
        print('Usuário não ativo no sistema. Verifique o access_token.')
    else:
        # Execução concluída com sucesso
        print('Chamada verificada com sucesso: ' + str(data['telefoneDestino']))
